import Koa, { Context } from "koa"

const app = new Koa();

app.use(async (ctx: Context) => {
  ctx.body = 'Hello world';
});

export default app;