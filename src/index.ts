import { config as envConfig } from "dotenv";
envConfig();

const { PORT } = process.env;

import Application from './app'

Application.listen(PORT, () => {
  console.log(`Koa server running at http://localhost:${PORT}`)
})